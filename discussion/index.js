// Selection Control Statements

	//if else statement
	//switch statement
	//try-catch-finally statement

//If-else
	/*
		syntax:
			if(condition){
				statement
			}
			else{
				statement
			}
	*/

let numA = -1;

if(numA<0){
	console.log("Hello")
};

console.log(numA<0);

if(numA>0){
	console.log("This statement will not be printed!");
}

console.log(numA>0)


let city = "New York"
if(city === 'New York'){
	console.log("Welcome to New York City")
}

let numB = 1;

if (numA>0){
	console.log("Hello")
}
else if(numB === 0){
	console.log("World");
}
else{
	console.log("Again");
}

/*
	Mini-activity
*/



function checkHeight(height){
if(height >= 150){
	console.log("Passed the minimum height requirement")
}
else{
	console.log("Did not pass min height requirement")
}
}
checkHeight();


let message = "No message"
console.log(message)

function determineTyphoon(windSpeed){
	if(windSpeed<30){
		return 'Not a typhoon yet.'
	}
	else if (windSpeed<=61){
		return 'Tropical depression detected.'
	}
	else if (windSpeed >=62 && windSpeed <=88) {
		return 'Tropical storm detected.'
	}
	else if(windSpeed>=89 && windSpeed <=117){
		return 'Sever tropical storm detected'
	}
	else{
		return 'Typhoon detected'
	}
}

message = determineTyphoon(70)
console.log(message);

if(message === "Tropical storm detected."){
	console.warn(message);
	console.error(message);
}

//Truthy and Falsy
/*
	-In javascript, a "truthy" value is a value that is considered TRUE when encountered in a boolean context
	-values are considered true unless defined otherwise:
	-Falsy values/exception for Truthy
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

if(true){
	console.log("Truthy");
}
if(1){
	console.log("Truthy");
}
if([]){
	console.log("Truthy");
}

//Falsy examples:
if(false){
	console.log("Falsy")
}
if(0){
	console.log("Falsy");
}
if(undefined){
	console.log("Falsy");
}

//Conditional (Ternary) Operator
/*
	Syntax:
		(expression) ? ifTrue : ifFalse;
*/

//Single statement execution
let ternaryResult = (1<18) ? true : false;
console.log("Result of ternary operator: " +ternaryResult);

//Multiple statement execution
let name;
function isOfLegalAge(){
	name = "John";
	return "You are of legal age"
}

function isUnderAge() {
	name = "Jane";
	return "You are under the age limit"
}

let age1 = parseInt(prompt("What is your age?"));
console.log(age1);
console.log(typeof age1);

let legalAge = (age1>16) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: "+ legalAge + ", " + name);

/*let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

switch(day) {
	case 'monday':
	console.log("The color of the day is red");
	break;
	case 'tuesday':
	console.log("The color of the day is orange");
	break;
	case 'wednesday':
	console.log("The color of the day is yellow");
	break;
	case 'thursday':
	console.log("The color of the day is green");
	break;
	case 'friday':
	console.log("The color of the day is blue"); break;
	case 'saturday':
	console.log("The color of the day is indigo"); break;
	case 'sunday':
	console.log("The color of the day is violet"); break;
	default:
	console.log("Please input a valid day"); break;
}*/

	
//Try Catch Finally Statements

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoon(windSpeed))
	}
	catch (error) {
		console.log(error)
		console.log(error.message)
	}
	finally {
		alert("Intensity updates will show new alert.")
	}
}

showIntensityAlert(56);